CREATE TABLE IF NOT EXISTS device (
    id bigserial PRIMARY KEY,
    created_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    updated_at timestamp(0) with time zone NOT NULL DEFAULT NOW(),
    name text not null,
    description text not null,
    allowed boolean NOT NULL DEFAULT false
)