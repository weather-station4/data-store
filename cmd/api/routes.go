package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (app *application) routes() *httprouter.Router {
	router := httprouter.New()

	router.NotFound = http.HandlerFunc(app.notFoundResponse)

	router.MethodNotAllowed = http.HandlerFunc(app.methodNotAllowedResponse)

	router.HandlerFunc(http.MethodGet, "/v1/healthcheck", app.healthcheckHandler)
	router.HandlerFunc(http.MethodPost, "/v1/devices", app.createDeviceHandler)
	router.HandlerFunc(http.MethodGet, "/v1/devices/:id", app.showDeviceHandler)
	// router.HandlerFunc(http.MethodPut, "/v1/devices/:id", app.updateDeviceHandler)
	router.HandlerFunc(http.MethodPatch, "/v1/devices/:id", app.updateDeviceHandler)
	router.HandlerFunc(http.MethodDelete, "/v1/devices/:id", app.deleteDeviceHandler)

	return router
}
