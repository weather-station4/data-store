package data

import (
	"database/sql"
	"errors"
)

var (
	ErrRecordNotFound = errors.New("record not found")
)

type Models struct {
	Device interface {
		Insert(device *Device) error
		Get(id int64) (*Device, error)
		Update(device *Device) error
		Delete(id int64) error
	}
}

func NewModels(db *sql.DB) Models {
	return Models{
		Device: DeviceModel{DB: db},
	}
}
