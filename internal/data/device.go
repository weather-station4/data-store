package data

import (
	"database/sql"
	"errors"
	"time"

	"gitlab.com/weather-station4/data-store/internal/validator"
)

type Device struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
	Allowed     bool      `json:"allowed"`
}

func ValidateDevice(v *validator.Validator, device *Device) {
	v.Check(device.Name != "", "name", "must be provided")
	v.Check(len(device.Name) <= 500, "name", "must not be more than 500 bytes long")

	v.Check(len(device.Description) <= 2048, "description", "must not be more than 2048 bytes long")
}

type DeviceModel struct {
	DB *sql.DB
}

func (dm DeviceModel) Insert(dev *Device) error {
	query := `
	INSERT INTO device (name, description, allowed)
	VALUES ($1, $2, $3)
	RETURNING id, created_at, updated_at`

	args := []any{dev.Name, dev.Description, dev.Allowed}

	return dm.DB.QueryRow(query, args...).Scan(&dev.ID, &dev.CreatedAt, &dev.UpdatedAt)
}

func (dm DeviceModel) Get(id int64) (*Device, error) {
	if id < 1 {
		return nil, ErrRecordNotFound
	}

	query := `
	select
		id
		, created_at
		, updated_at
		, name
		, description
		, allowed
	from
		device
	where
		id = $1`

	var dev Device

	err := dm.DB.QueryRow(query, id).Scan(
		&dev.ID,
		&dev.CreatedAt,
		&dev.UpdatedAt,
		&dev.Name,
		&dev.Description,
		&dev.Allowed,
	)

	if err != nil {
		switch {
		case errors.Is(err, sql.ErrNoRows):
			return nil, ErrRecordNotFound
		default:
			return nil, err
		}
	}

	return &dev, nil
}

func (dm DeviceModel) Update(dev *Device) error {

	query := `
	UPDATE
		device
	SET
		name = $1
		, description = $2
		, allowed = $3
		, updated_at = NOW()
	WHERE
		id = $4
	RETURNING
		updated_at`

	args := []any{
		dev.Name,
		dev.Description,
		dev.Allowed,
		dev.ID,
	}

	return dm.DB.QueryRow(query, args...).Scan(&dev.UpdatedAt)
}

func (dm DeviceModel) Delete(id int64) error {
	if id < 1 {
		return ErrRecordNotFound
	}

	query := `
	DELETE FROM device
	WHERE id = $1`

	result, err := dm.DB.Exec(query, id)
	if err != nil {
		return err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrRecordNotFound
	}
	return nil
}
